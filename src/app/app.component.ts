import { Component, OnInit, DoCheck, AfterViewInit, OnDestroy, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<app-sub [test]="1"></app-sub>`
})
export class AppComponent { }



@Component({
  selector: 'app-sub',
  template: ``
})
export class AppSubComponent implements OnInit, DoCheck, AfterViewInit, OnDestroy, OnChanges {
  @Input('test') test: any;
  constructor() {
    console.log('in constructor');
  }

  ngOnInit(): void {
    console.log('in ngOnInit');
  }

  ngDoCheck(): void {
    console.log('in ngDoCheck');
  }

  ngAfterViewInit(): void {
    console.log('in ngAfterViewInit', this.test);
  }

  ngOnDestroy(): void {
    console.log('in ngOnDestroy');
  }

  ngOnChanges(): void {
    console.log('in ngOnChanges ');
  }
}
